""pathogen plugin
filetype off " Pathogen needs to run before plugin indent on
call pathogen#incubate()
call pathogen#helptags()
filetype on
filetype plugin indent on

"" syntax highlighting
syntax on


""cf3 highlighting
au BufRead,BufNewFile *.cf set ft=cf3


""set list
""set listchars=tab:>.,trail:.,extends:#,nbsp:.


"map <up> <nop>
"map <down> <nop>
"map <left> <nop>
"map <right> <nop>


nnoremap j gj
nnoremap k gk

