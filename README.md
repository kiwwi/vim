VIM settings
============



Installing your Vim environment on another machine
--------------------------------------------------

Once your vim configuration is under version control, it’s quite straightforward to import your settings to any machine that has git installed. If you followed the instructions above to put your vimrc and plugins in a dotvim directory, then you can follow these steps to synchronise them to another machine:

    $cd ~
    $git clone https://kiwwi@bitbucket.org/kiwwi/vim.git ~/.vim
    $ln -s ~/.vim/.vimrc ~/.vimrc
    $cd ~/.vim
    $git submodule init
    $git submodule update

The last two git commands can be rolled in to one: git submodule update --init.

Upgrading a plugin bundle
-------------------------

At some point in the future, a plugin might be updated. To fetch the latest changes, go into the plugin repository, and pull the latest version:

    $cd ~/.vim/bundle/plugin_name
    $git pull origin master


Upgrading all bundled plugins
-----------------------------
You can use the foreach command to execute any shell script in from the root of all submodule directories. To update to the latest version of each plugin bundle, run the following:

     $git submodule foreach git pull origin master
